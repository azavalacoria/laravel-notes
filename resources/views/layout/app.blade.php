<html>
<head>
    <title>@yield('title')</title>
    <link rel="stylesheet" href="/css/app.css" />
    <script type="text/javascript" src="/js/app.js"></script> 
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">{{config('app.name')}}</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="/notes">Notas 
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Acciones <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="/notes/create">Agregar Nota</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="/contact">Contacto
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#">Link</a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <div class="container">
        <div id="system-messages"class="row">
            @yield('system_messages')
        </div>
        <div class="row">
            @yield('content')
        </div>
    </div>
    
<footer class="page-footer center-on-small-only">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                @yield('options')
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container-fluid text-center">
            <strong>{{ date('Y') }} - {{ config('app.name') }}</strong>
        </div>
    </div>
    <!--/.Copyright-->

</footer>

</body>
</html>