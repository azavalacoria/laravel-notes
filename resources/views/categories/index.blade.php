@extends('layout/app')
@section('title')
    Listado de Categorías
@endsection
@section('content')
	Mostrando {{$size}}
    @foreach ($categories as $category)
    <div>
		<a href="categories/{{ $category->id }}">
			<h2>{{ $category->name }}</h2>
		</a>
		<code>
			<a href="categories/edit/{{$category->id}}">
				<small>Edit</small>
			</a>
		</code>>
	</div>
	@endforeach
@endsection