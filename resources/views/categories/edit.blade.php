@extends('layout/app')
@section('title')
    Editar Categoría
@endsection
@section('content')
		
	<form action="/categories/update" method="POST" role="form">
		{{ csrf_field() }}
		<legend>Editando categoría: {{$category->name}}</legend>
		
		<div>
			<ul>
				@foreach($errors->all() as $error)
					<li class="alert alert-danger">{{$error}}</li>
				@endforeach
			</ul>
		</div>

		<div class="form-group">
			<label>Nombre</label>
			<input type="text" name="name" class="form-control" value="{{$category->name}}">
		</div>
		
		<div class="form-group">
			<label>Activa: </label> &nbsp
			<input type="checkbox" name="active" value="1" checked="true">
		</div>
		
		<button type="submit" class="btn btn-primary">Agregar</button>
	</form>
@endsection