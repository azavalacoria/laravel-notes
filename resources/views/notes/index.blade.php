@extends('layout/app')
@section('title')
	{{'Listado de Notas Creadas'}}
@endsection
@section('content')
    <div class="col-xs-12">
    	<div class="col-xs-10">
    		@include('notes._display')
    	</div>
    	<div class="col-xs-2 text-right">
    		<a href="notes/create" class="glyphicon glyphicon-plus btn btn-success btn-xl" data-toggle="tooltip" data-placement="top" title="Agregar nueva nota">
    		</a>
    	</div>
    </div>
@endsection

@section('options')
<div class="col-md-offset-6 text-center">
	<div class="dropdown">
		<button class="btn btn-default dropdown-toggle" type="button" id="other-options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Más opciones <span class="caret"></span></button>
		<ul class="dropdown-menu" aria-labelledby="other-options">
			<li>
				<a href="/notes/recycler" class="glyphicon glyphicon-trash text-danger">
					Papelera
				</a>
			</li>
		</ul>
	</div>
</div>
@endsection