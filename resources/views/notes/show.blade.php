@extends('layout/app')
@section('title')
    Información de la nota {{$note->title}}
@endsection
@section('content')
<div class="row">
	<div class="col-md-9">
		<h1>{{$note->title}}</h1>
	</div>
	<div class="col-md-3 text-right">
		<a href="/notes/{{ $note->id }}/edit" class="glyphicon glyphicon-edit btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Editar {{ $note->title }}"">
		</a>
		
		<a href="#" class="glyphicon glyphicon-remove btn btn-danger btn-sm"></a>
	</div>
</div>
<div class="row">
	@if($note->isImportant())
		<strong>Important</strong>
	@endIf
	<pre>{{$note->body}}</pre>
</div>

@endsection