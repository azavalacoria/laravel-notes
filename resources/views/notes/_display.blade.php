<div class="row">
	<h1>Listado de notas {{ isset($recycler) ? 'borradas': ''}}</h1>
	<br>
</div>
@foreach ($notes as $note)
	<div class="row">
		<div class="col-xs-12 thumbnail">
    		<div class="row">
    			<div class="col-xs-8">
					<a href="notes/{{ $note->id }}">
						@if($note->important)
							<h2>{{ $note->title }}</h2>
						@elseif(!$note->important)
							<p>{{ $note->title }}</p>
						@endif
					</a>
				</div>
	    		<div class="col-xs-4 text-right">
	    			<a href="/notes/{{ $note->id }}/{{ isset($recycler) ? 'recover' : 'edit' }}" class="glyphicon glyphicon-{{ isset($recycler) ? 'ok-sign' : 'edit' }} btn btn-{{ isset($recycler) ? 'success' : 'warning' }} btn-sm" data-toggle="tooltip" data-placement="top" title="Recuperar {{ $note->title }}">
	    			</a>
	    			@include('notes._delete_form')
	    		</div>
    		</div>
    		<div class="row">
    			<div class="col-xs-10 col-xs-offset-0">
    				<span>{{ $note->body }}</span>
    			</div>
    			
    		</div>
    	</div>
	</div>
@endforeach