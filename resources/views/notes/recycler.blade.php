@extends('layout/app')
@section('title')
    @isset($notes)
    	{{'Listado de Notas Borradas'}}
    @endisset
@endsection
@section('content')
	<div class="col-xs-12">
		@include('notes._display')
	</div>
    
@endsection
