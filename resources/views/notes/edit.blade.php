@extends('layout/app')
@section('title')
    Editar Nota: {{ $note->title }}
@endsection
@section('content')
	
	<form action="/notes/{{ $note->id }}/update" method="POST" role="form">
		{{ method_field('PATCH') }}
		{{ csrf_field() }}

		<legend>Editar nota</legend>
		
		@include('notes._form')
		
		
	</form>	
	
@endsection