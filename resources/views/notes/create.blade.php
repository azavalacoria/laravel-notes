@extends('layout/app')
@section('title')
    Nueva nota
@endsection
@section('content')
	<form action="/notes" method="POST" role="form">

		{{ csrf_field() }}

		<legend>Crear nueva nota</legend>

		@include('notes._form')
		
	</form>
@endsection