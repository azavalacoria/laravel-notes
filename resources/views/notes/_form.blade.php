<div class="horizontal-form">
	<div class="form-group">
		<div class="col-sm-12"><br></div>
		<label for="input-title" class="col-sm-2 control-label">Título</label>
		<div class="col-sm-10 control-label">
			<input type="text" name="title" class="form-control" value="{{ isset ($note) ? $note->title : '' }}" id="input-title" placeholder="New title">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-12"><br></div>
		<label for="input-body" class="col-sm-2 control-label">Contenido</label>
		<div class="col-sm-10 control-label">
			<textarea name="body" id="input-body" class="form-control" placeholder="Content of the new note">{{ isset($note) ? $note->body : '' }}</textarea>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-12"><br></div>
		<label for="input-important" class="col-sm-2 control-label">Es importante: </label>
		<div class="col-sm-10 control-label">
			<label>

				<input type="checkbox" class="form-control" name="important" value="1" 
				@isset($note)
					{{ $note->isImportant() ? 'checked' : '' }}
				@endisset >
			</label>
			
		</div>
		
	</div>
</div>

<br>

<div class="text-right">
	<button type="submit" class="btn btn-success">
		{{ isset($note) ? 'Actualizar' : 'Guardar' }}
	</button> 
	<a href="/notes" class="btn btn-warning"> Cancelar</a>
</div>

