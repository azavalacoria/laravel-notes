<button type="button" class="glyphicon glyphicon-remove btn btn-danger btn-sm" data-toggle="modal" data-target=".bs-example-modal-sm-{{ $note->id }}" data-toggle="tooltip" data-placement="top" title="Borrar {{ $note->title }}"></button>

<div class="modal fade bs-example-modal-sm-{{ $note->id }}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="my-modal-{{ $note->id }}">
  <div class="modal-dialog modal-sm" role="document">
	<div class="modal-content">
	  <div class="modal-header text-center">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">Confirmar acción</h4>
	  </div>
	  <div class="modal-body text-justify">
	  	<p>
	  		¿Desea {{ isset($recycler) ? 'eliminar por completo ' : 'borrar'}} la <strong>{{ $note->title }}</strong>? @if (isset($recycler))
	  			Una vez hecho esto, ya no podrá recuperarla.
	  		@endif
	  	</p>
		
	  </div>
	  <div class="modal-footer">
	  	<form action="/notes/{{ $note->id }}/{{ isset($recycler) ? 'remove' : 'delete'}}" method="post" role="form">
	  		{{ method_field('DELETE') }}
			{{ csrf_field() }}
	  		<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	  		<button type="submit" class="btn btn-danger">Eliminar</button>
	  	</form>
	  </div>
	</div>
  </div>
</div>