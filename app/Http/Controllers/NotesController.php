<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;

class NotesController extends Controller
{



	public function index()
	{
		$notes = Note::where('active', true)->where('deleted', false)->get();
		return view('notes/index', compact('notes'));
	}

	public function show(Note $note)
	{
		if (isset($note)) {
			return view('notes/show', compact('note'));
		} else {
			redirect('/notes');
		}
	}

	public function create()
	{
		return view('notes.create');
	}

	public function edit(Note $note)
	{
		return view('notes.edit', compact('note'));
	}

	public function update(Note $note, Request $request)
	{
		$note->title = request()->title;
		$note->body = request()->body;
		$note->important = is_null( request()->important) ? 0 :1;
		$note->save();
		return redirect('/notes');
	}

	public function store()
	{
		$note = new Note;
		$note->active = true;
		$note->deleted = false;
		$note->title = request()->title;
		$note->body = request()->body;
		$note->important = is_null( request()->important) ? 0 :1;
		$note->save(); 
		
		return redirect('notes/create');
	}

	public function delete(Note $note)
	{
		$note->active = false;
		$note->deleted = true;

		$note->save();

		return redirect('notes/');
	}

	public function remove(Note $note)
	{
		//echo "a eliminar $note->id";
		$note->delete();

		redirect('notes/recycler');
	}

	public function recycler()
	{
		$notes = Note::where('active', false)->where('deleted', true)->get();
		$recycler = true;
		return view('notes/recycler', compact('notes', 'recycler'));
	}

	public function recover(Note $note)
	{
		$note->active = true;
		$note->deleted = false;

		$note->save();

		return redirect('notes/recycler');
	}
}
