<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoriesController extends Controller
{
    //

    public function index()
    {
    	$categories = Category::where('active', true)->orderBy('name', 'asc')->get();
    	$size = sizeof($categories);
    	return view('categories/index', ['categories' => $categories, 'size' => $size]);
    }

    public function show(Category $category)
    {
    	return view('categories/show', ['category' => $category]);
    }

    public function create()
    {
    	return view('categories/create');
    }

    public function store()
    {
    	$this->validate(request(), [
    		'name' => 'required|min:3|max:15'
    	] );
    	
    	$category = new Category;
    	$category->name = request()->name;
    	$category->active = is_null( request()->active) ? 0 :1;
    	$category->deleted = 0;
		$category->save();
    	return view('/categories/create');
    }

    public function edit(Category $category)
    {
    	//dd($category);

    	return view('/categories/edit', ['category' => $category]);
    }
}
