<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/contact', function() {
    return view('contact');
});

Route::get('/notes', 'NotesController@index');

Route::get('notes/recycler', 'NotesController@recycler');

Route::get('/notes/create', 'NotesController@create');

Route::get('/notes/{note}', 'NotesController@show');

Route::get('/notes/{note}/edit', 'NotesController@edit');

Route::patch('/notes/{note}/update', 'NotesController@update');

Route::delete('/notes/{note}/delete', 'NotesController@delete');

Route::delete('/notes/{note}/remove', 'NotesController@remove');

Route::get('/notes/{note}/recover', 'NotesController@recover');

Route::post('/notes/', 'NotesController@store');



Route::get('/categories', 'CategoriesController@index' );

Route::get('/categories/create', 'CategoriesController@create');

Route::get('/categories/{category}', 'CategoriesController@show');

Route::get('categories/edit/{category}', 'CategoriesController@edit');

Route::post('categories', 'CategoriesController@store');

Route::post('categories/update', 'CategoriesController@store');


